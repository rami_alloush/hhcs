package net.rmasoft.hhcs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

@SuppressWarnings("unchecked")
public class PatientAdapter extends FirestoreRecyclerAdapter<Patient, PatientAdapter.PatientHolder> {

    private Context context;

    PatientAdapter(@NonNull FirestoreRecyclerOptions options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull PatientHolder holder, int position, @NonNull Patient model) {
        final Patient currentPatient = model;
        final int currentPosition = position;
        final String fName = model.getFname();
        final String lName = model.getLname();
        holder.mFullName.setText(model.toString());
        holder.mEmail.setText(model.getEmail());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPatient.setUID(getSnapshots().getSnapshot(currentPosition).getId());
                Intent labTestIntent = new Intent(context, AddLabTestActivity.class);
                labTestIntent.putExtra("patient", currentPatient);
                context.startActivity(labTestIntent);
            }
        });
    }

    @NonNull
    @Override
    public PatientHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_user_item, parent, false);
        context = parent.getContext();
        return new PatientHolder(view);
    }

    class PatientHolder extends RecyclerView.ViewHolder {
        private TextView mFullName;
        private TextView mEmail;
        private View mView;

        PatientHolder(View view) {
            super(view);
            mFullName = view.findViewById(R.id.full_name);
            mEmail = view.findViewById(R.id.email);
            mView = view;
        }
    }
}
