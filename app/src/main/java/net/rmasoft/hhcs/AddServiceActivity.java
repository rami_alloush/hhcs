package net.rmasoft.hhcs;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class AddServiceActivity extends AppCompatActivity {

    private String patientUID;
    private String serviceLocation;
    private String serviceTime;
    private EditText reachDetailsView;
    private Button setTimeBtn;
    private Spinner mySpinner;
    private Calendar now = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            setTimeBtn.setText(String.valueOf(String.valueOf(year) + '/' + monthOfYear + '/' + dayOfMonth));
            new TimePickerDialog(AddServiceActivity.this, timePickerListener,
                    now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false).show();
        }

    };
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @SuppressLint("SimpleDateFormat")
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            try {
                Date dateObj = new SimpleDateFormat("H:mm").parse(String.valueOf(String.valueOf(hourOfDay) + ':' + minute));
                String formattedTime = new SimpleDateFormat("K:mm a").format(dateObj);
                serviceTime = setTimeBtn.getText() + " " + formattedTime;
                setTimeBtn.setText(serviceTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);

        // initializing firebase auth object and get current user UID
        patientUID = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();

        // Get service location
        serviceLocation = Objects.requireNonNull(getIntent().getExtras()).getString("serviceLocation");

        // Read from input fields
        EditText serviceReachAddress = findViewById(R.id.serviceReachAddress);
        EditText serviceReachPhone = findViewById(R.id.serviceReachPhone);

        if (serviceLocation.equals("online")) {
            reachDetailsView = serviceReachPhone;
            serviceReachAddress.setVisibility(View.GONE);
            findViewById(R.id.serviceReachAddressLabel).setVisibility(View.GONE);

        } else if (serviceLocation.equals("offline")) {
            reachDetailsView = serviceReachAddress;
            serviceReachPhone.setVisibility(View.GONE);
            findViewById(R.id.serviceReachPhoneLabel).setVisibility(View.GONE);
        }

        // Create ArrayAdapter and provide Empty Doctors ArrayList
        final ArrayAdapter<Doctor> mySpinnerArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, new ArrayList<Doctor>());
        mySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Get doctors from Firestore and them to the Adapter
        FirebaseFirestore.getInstance()
                .collection("users")
                .whereEqualTo("type", "doctor")
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                        Doctor doctor = document.toObject(Doctor.class);
                        doctor.setUID(document.getId());
                        mySpinnerArrayAdapter.add(doctor);
                    }
                }
            }
        });

        // Assign Adapter and OnClickListener to the Spinner
        mySpinner = findViewById(R.id.doctorUID);
        mySpinner.setAdapter(mySpinnerArrayAdapter); // Set the custom adapter to the spinner

        // Add action for the addServiceBtn
        Button addService = findViewById(R.id.addServiceBtn);
        addService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addService();
            }
        });

        // Set date and time of service
        setTimeBtn = findViewById(R.id.setTimeBtn);
        setTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open date and time pickers on current date and time
                DatePickerDialog myDate = new DatePickerDialog(AddServiceActivity.this, datePickerListener,
                        now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                // force future dates only
                myDate.getDatePicker().setMinDate(now.getTimeInMillis());
                myDate.show();
            }
        });
    }

    private void addService() {
        String reachDetails = reachDetailsView.getText().toString().trim();
        String doctorUIDSelected = ((Doctor) mySpinner.getSelectedItem()).getUID();

        // Check if all the fields in the editor are filled
        if (TextUtils.isEmpty(reachDetails) || TextUtils.isEmpty(serviceTime)
                || TextUtils.isEmpty(doctorUIDSelected)) {
            Toast.makeText(this, R.string.fill_all_fields,
                    Toast.LENGTH_LONG).show();
            // make sure we don't continue the code
            return;
        }

        FirebaseFirestore.getInstance().collection("Services")
                .add(new Service(patientUID, doctorUIDSelected, serviceLocation, reachDetails, serviceTime, false))
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(AddServiceActivity.this,
                                R.string.service_add, Toast.LENGTH_LONG).show();
                    }
                });
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return (super.onOptionsItemSelected(item));
    }
}
