package net.rmasoft.hhcs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddLabTestActivity extends AppCompatActivity {

    private String TAG = "AddPatientActivity";
    /**
     * Declare the TextViews
     */
    private EditText firstNameView;
    private EditText lastNameView;
    private EditText emailView;
    private EditText passwordView;
    private TextView patientEmailLabel;
    private TextView patientPasswordLabel;
    private ImageView labTestImageView;
    private Bitmap bitmap;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Patient currentPatient;

    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;
    private FirebaseUser newUser;

    private SharedPreferences adminPref;
    private SharedPreferences.Editor adminPrefEditor;
    private boolean isAdmin;
    private int RESULT_LOAD_IMG = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        // Find relevant views that will display data
        firstNameView = findViewById(R.id.patientFirstName);
        lastNameView = findViewById(R.id.patientLastName);
        patientEmailLabel = findViewById(R.id.patientEmailLabel);
        emailView = findViewById(R.id.patientEmail);
        patientPasswordLabel = findViewById(R.id.patientPasswordLabel);
        passwordView = findViewById(R.id.patientPassword);
        labTestImageView = findViewById(R.id.labTestImage);

        // Disable password edit
        patientEmailLabel.setVisibility(View.INVISIBLE);
        emailView.setVisibility(View.INVISIBLE);
        patientPasswordLabel.setVisibility(View.INVISIBLE);
        passwordView.setVisibility(View.INVISIBLE);

        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();

        // Save current position in ViewPager
        adminPref = getPreferences(MODE_PRIVATE);
        adminPrefEditor = adminPref.edit();
        adminPrefEditor.putBoolean("restorePosition", true);
        adminPrefEditor.apply();

        // Populate selected Patient info
        currentPatient = getIntent().getParcelableExtra("patient");
        firstNameView.setText(currentPatient.getFname());
        lastNameView.setText(currentPatient.getLname());
        emailView.setText(currentPatient.getEmail());
        passwordView.setText(currentPatient.getPassword());
        // Reference to an image file in Cloud Storage
        StorageReference storageReference = FirebaseStorage.getInstance().
                getReference()
                .child(currentPatient.getUID() + ".jpg");

        // Download directly from StorageReference using Glide
        GlideApp.with(this)
                .load(storageReference)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.img_placeholder)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true))
                .into(labTestImageView);
        Log.i(TAG, "Glide1 Loaded");

        Button loadImage = findViewById(R.id.loadlabTest);
        loadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            }
        });

        Button setChanges = findViewById(R.id.setChanges);
        setChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePatient(currentPatient.getUID());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // When an Image is picked
        if (requestCode == RESULT_LOAD_IMG) {

            if (resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                try {
                    assert selectedImage != null;
                    bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
                    labTestImageView.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this, R.string.no_image_picked, Toast.LENGTH_LONG).show();
            }
        }
    }

    // Menu Methods
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add(Menu.NONE, 0, Menu.NONE, getString(R.string.save));
        item.setIcon(android.R.drawable.ic_menu_save);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                updatePatient(currentPatient.getUID());
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
    Register User with FirebaseAuth
     */
    private void updateFirebaseUser() {
        // Connect to Firebase Auth to create new user using Email and Password
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {
                            // Sign in success
                            //display some message here
                            Toast.makeText(getBaseContext(), "Successfully registered", Toast.LENGTH_LONG).show();
                            newUser = firebaseAuth.getCurrentUser();
                            if (firebaseAuth.getCurrentUser() != null) {
                                // Update Display Name
                                String fullName = firstName + " " + lastName;
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(fullName).build();
                                if (newUser != null) {
                                    newUser.updateProfile(profileUpdates);
                                }
                                updatePatient(newUser.getUid());
                            }

                        } else {
                            //display some message here
                            Toast.makeText(getBaseContext(), "Registration Error: " + Objects.requireNonNull(task.getException()).getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    /**
     * Save new Patient from EditText fields to Firestore
     */
    private void updatePatient(String newUID) {
        // Read from input fields
        // Use trim to eliminate leading or trailing white space
        firstName = firstNameView.getText().toString().trim();
        lastName = lastNameView.getText().toString().trim();
        email = emailView.getText().toString().trim();
        password = passwordView.getText().toString().trim();

        // Check if all the fields in the editor are filled
        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName)
                && TextUtils.isEmpty(email) && TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.fill_all_fields,
                    Toast.LENGTH_LONG).show();
            // make sure we don't continue the code
            return;
        }

        //checking if any field is empty
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)) {
            Toast.makeText(this, R.string.please_enter_name, Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(email) || !isEmailValid(email)) {
            Toast.makeText(this, R.string.enter_valid_email, Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.please_enter_password, Toast.LENGTH_LONG).show();
            return;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference patientRef = db.collection("users").document(newUID);
        //  SetOptions.merge() as we are not updating the whole Patient object
        patientRef.set(new Patient(firstName, lastName, email, password), SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(AddLabTestActivity.this, R.string.patient_updated, Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });

        // Check if new image loaded
        if(bitmap != null) {
            // Get the data from an ImageView as bytes
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            // Upload lab test image to Firestore Storage
            StorageReference mountainsRef = FirebaseStorage.getInstance().getReference(newUID + ".jpg");
            UploadTask uploadTask = mountainsRef.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                }
            });
        }
        finish();
    }

    public boolean signAdmin() {
        // Connect to Firebase Auth to sign in user using Email and Password
        String email = "admin@hospital.com";
        String password = "123456";
        boolean result = false;
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {
                            boolean result = true;
                        }
                    }
                });
        return result;
    }

    /**
     * method is used for checking valid email id format.
     */
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}