package net.rmasoft.hhcs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

@SuppressWarnings("unchecked")
public class DoctorAdapter extends FirestoreRecyclerAdapter<Doctor, DoctorAdapter.DoctorHolder> {

    private Context context;

    DoctorAdapter(@NonNull FirestoreRecyclerOptions options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull DoctorHolder holder, int position, @NonNull Doctor model) {
        final int current = position;
        final String fName = model.getFname();
        holder.mFullName.setText(model.toString());
        holder.mEmail.setText(model.getEmail());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "Doctor No. " + (current + 1), Toast.LENGTH_LONG).show();
            }
        });
    }

    @NonNull
    @Override
    public DoctorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_user_item, parent, false);
        context = parent.getContext();
        return new DoctorHolder(view);
    }

    class DoctorHolder extends RecyclerView.ViewHolder {
        private TextView mFullName;
        private TextView mEmail;
        private View mView;

        DoctorHolder(View view) {
            super(view);
            mFullName = view.findViewById(R.id.full_name);
            mEmail = view.findViewById(R.id.email);
            mView = view;
        }
    }
}
