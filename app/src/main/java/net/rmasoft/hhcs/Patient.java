package net.rmasoft.hhcs;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class Patient implements Parcelable {
    private String mFname;
    private String mLname;
    private String mEmail;
    private String mPassword;
    private String mUID;

    public Patient() {
    } // Needed for Firebase

    public Patient(String firstName, String lastName, String email, String password) {
        mFname = firstName;
        mLname = lastName;
        mEmail = email;
        mPassword = password;
    }

    String getFname() {
        return mFname;
    }

    public void setFname(String name) {
        mFname = name;
    }

    String getLname() {
        return mLname;
    }

    public void setLname(String name) {
        mLname = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getType() {
        return "patient";
    }

    String getUID() {
        return mUID;
    }

    void setUID(String mUID) {
        this.mUID = mUID;
    }

    @NonNull
    @Override
    public String toString() {
        return mFname + " " + mLname;
    }


    protected Patient(Parcel in) {
        mFname = in.readString();
        mLname = in.readString();
        mEmail = in.readString();
        mPassword = in.readString();
        mUID = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFname);
        dest.writeString(mLname);
        dest.writeString(mEmail);
        dest.writeString(mPassword);
        dest.writeString(mUID);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Patient> CREATOR = new Parcelable.Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel in) {
            return new Patient(in);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };
}