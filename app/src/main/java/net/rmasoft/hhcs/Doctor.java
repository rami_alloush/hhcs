package net.rmasoft.hhcs;

import android.support.annotation.NonNull;

public class Doctor {
    private String mFname;
    private String mLname;
    private String mEmail;
    private String mPassword;
    private String mSpeciality;
    private String mShiftInfo;
    private String mUID;


    public Doctor() {
    } // Needed for Firebase

    Doctor(String firstName, String lastName, String email, String password, String speciality, String shiftInfo) {
        mFname = firstName;
        mLname = lastName;
        mEmail = email;
        mPassword = password;
        mSpeciality = speciality;
        mShiftInfo = shiftInfo;
    }

    String getFname() {
        return mFname;
    }

    public void setFname(String name) {
        mFname = name;
    }

    String getLname() {
        return mLname;
    }

    public void setLname(String name) {
        mLname = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getSpeciality() {
        return mSpeciality;
    }

    public void setSpeciality(String speciality) {
        mSpeciality = speciality;
    }

    public String getShiftInfo() {
        return mShiftInfo;
    }

    public void setShiftInfo(String shiftInfo) {
        mShiftInfo = shiftInfo;
    }

    public String getType() {
        return "doctor";
    }

    public String getUID() {
        return mUID;
    }

    public void setUID(String mUID) {
        this.mUID = mUID;
    }

    // Used in Spinner Display
    @NonNull
    @Override
    public String toString() {
        return mFname + " " + mLname;
    }
}