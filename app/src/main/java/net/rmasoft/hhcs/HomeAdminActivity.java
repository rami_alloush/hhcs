package net.rmasoft.hhcs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class HomeAdminActivity extends AppCompatActivity {

    private static final String TAG = "HomeAdminActivity";

    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;

    // Fragments components
    private HomeAdminActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private FloatingActionButton fab;
    private SharedPreferences adminPref;
    private SharedPreferences.Editor adminPrefEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_admin);

        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new HomeAdminActivity.SectionsPagerAdapter(getSupportFragmentManager());

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPage = mViewPager.getCurrentItem();
                if (currentPage == 1) {
                    Intent addPatient = new Intent(getBaseContext(), AddPatientActivity.class);
                    addPatient.putExtra("isAdmin", true);
                    startActivity(addPatient);

                } else if (currentPage == 2) {
                    Intent addPatient = new Intent(getBaseContext(), AddDoctorActivity.class);
                    startActivity(addPatient);
                }
            }
        });
        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fab.hide();
                        break;

                    default:
                        fab.show();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        // Give the TabLayout the ViewPager
        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        if (savedInstanceState != null) {
            mViewPager.setCurrentItem(savedInstanceState.getInt("pageItem", 0));
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a HomeFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new ServicesFragment();
                case 1:
                    return new PatientsFragment();
                case 2:
                    return new DoctorsFragment();
                default:
                    return new ServicesFragment();
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.title_home);
                case 1:
                    return getString(R.string.patients);
                case 2:
                    return getString(R.string.doctors);
                default:
                    return "";
            }
        }
    }

    // Menu Functions
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add(Menu.NONE, 0, Menu.NONE, getString(R.string.sign_out));
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                userSingOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Save current position in ViewPager
        adminPref = getPreferences(MODE_PRIVATE);
        adminPrefEditor = adminPref.edit();
        adminPrefEditor.putInt("viewPagerPage", mViewPager.getCurrentItem());
        adminPrefEditor.putBoolean("restorePosition", true);
        adminPrefEditor.apply();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Return to saved position in ViewPager
        adminPref = getPreferences(MODE_PRIVATE);
        int value = adminPref.getInt("viewPagerPage", 0);
        boolean restorePosition = adminPref.getBoolean("restorePosition", false);
        if (restorePosition) {
            mViewPager.setCurrentItem(value);
        }
    }

    /*
        Helper method for user sign ou
         */
    public void userSingOut() {
        try {
            firebaseAuth.signOut();
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}