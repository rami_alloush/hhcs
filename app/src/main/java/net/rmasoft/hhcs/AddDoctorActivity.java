package net.rmasoft.hhcs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddDoctorActivity extends AppCompatActivity {

    /**
     * Declare the TextViews
     */
    private EditText firstNameView;
    private EditText lastNameView;
    private EditText emailView;
    private EditText passwordView;
    private EditText specialityView;
    private EditText shiftInfoView;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String speciality;
    private String shiftInfo;

    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;
    private FirebaseUser newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_doctor);

        // Find relevant views that will display data
        firstNameView = findViewById(R.id.doctorFirstName);
        lastNameView = findViewById(R.id.doctorLastName);
        emailView = findViewById(R.id.doctorEmail);
        passwordView = findViewById(R.id.doctorPassword);
        specialityView = findViewById(R.id.doctorSpeciality);
        shiftInfoView = findViewById(R.id.doctorShiftInfo);

        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();

    }

    // Menu Methods
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add(Menu.NONE, 0, Menu.NONE, getString(R.string.save));
        item.setIcon(android.R.drawable.ic_menu_save);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                registerUser();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
    Register User with FirebaseAuth
     */
    private void registerUser() {
        // Read from input fields
        // Use trim to eliminate leading or trailing white space
        firstName = firstNameView.getText().toString().trim();
        lastName = lastNameView.getText().toString().trim();
        email = emailView.getText().toString().trim();
        password = passwordView.getText().toString().trim();
        speciality = specialityView.getText().toString().trim();
        shiftInfo = shiftInfoView.getText().toString().trim();

        // Check if all the fields in the editor are filled
        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName)
                && TextUtils.isEmpty(email) && TextUtils.isEmpty(password)
                && TextUtils.isEmpty(speciality) && TextUtils.isEmpty(shiftInfo)) {
            Toast.makeText(this, R.string.fill_all_fields,
                    Toast.LENGTH_LONG).show();
            // make sure we don't continue the code
            return;
        }

        //checking if any field is empty
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)) {
            Toast.makeText(this, R.string.please_enter_name, Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(email) || !isEmailValid(email)) {
            Toast.makeText(this, R.string.enter_valid_email, Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.please_enter_password, Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(speciality) || TextUtils.isEmpty(shiftInfo)) {
            Toast.makeText(this, R.string.fill_all_fields, Toast.LENGTH_LONG).show();
            return;
        }

        // Connect to Firebase Auth to create new user using Email and Password
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {
                            // Sign in success
                            //display some message here
                            Toast.makeText(getBaseContext(), "Successfully registered", Toast.LENGTH_LONG).show();
                            newUser = firebaseAuth.getCurrentUser();
                            if (firebaseAuth.getCurrentUser() != null) {
                                // Update Display Name
                                String fullName = firstName + " " + lastName;
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(fullName).build();
                                if (newUser != null) {
                                    newUser.updateProfile(profileUpdates);
                                }
                                saveDoctor(newUser.getUid());
                            }

                        } else {
                            //display some message here
                            Toast.makeText(getBaseContext(), "Registration Error: " + task.getException().getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    /**
     * Save new Doctor from EditText fields to Firestore
     */
    private void saveDoctor(String newUID) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference doctorRef = db.collection("users").document(newUID);
        doctorRef.set(new Doctor(firstName, lastName, email, password, speciality, shiftInfo))
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(AddDoctorActivity.this, R.string.doctor_add, Toast.LENGTH_LONG).show();
                        signAdmin();
                    }
                });
        finish();
    }

    public boolean signAdmin() {
        // Connect to Firebase Auth to sign in user using Email and Password
        String email = "admin@hospital.com";
        String password = "123456";
        boolean result = false;
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {
                            boolean result = true;
                        }
                    }
                });
        return result;
    }

    /**
     * method is used for checking valid email id format.
     */
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}