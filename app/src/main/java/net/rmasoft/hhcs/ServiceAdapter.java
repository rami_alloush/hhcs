package net.rmasoft.hhcs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

@SuppressWarnings("unchecked")
public class ServiceAdapter extends FirestoreRecyclerAdapter<Service, ServiceAdapter.ServiceHolder> {

    private Context context;
    private Boolean canRate;
    private Boolean newService;

    ServiceAdapter(@NonNull FirestoreRecyclerOptions options, Boolean canRate, Boolean newService) {
        super(options);
        this.canRate = canRate;
        this.newService = newService;
    }

    @Override
    protected void onBindViewHolder(@NonNull ServiceHolder holder, int position, @NonNull Service model) {
        final int current = position;
        final Service service = model;
        String serviceLocation;
        if (model.getLocation().equals("online")) {
            serviceLocation = context.getString(R.string.service_type_online);
        } else {
            serviceLocation = context.getString(R.string.service_type_offline);
        }
        holder.serviceType.setText(serviceLocation);
        holder.serviceDetails.setText(model.getReachDetails());
        holder.serviceTime.setText(model.getTime());
        if (model.getRate() != null) {
            holder.serviceRate.setRating(model.getRate());
        }
        if (canRate) {
            holder.serviceRate.setIsIndicator(false);
        }
        if (newService) {
            holder.serviceRate.setVisibility(View.GONE);
            holder.serviceComplete.setVisibility(View.VISIBLE);
            holder.serviceComplete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        service.setCompleted(true);
                        FirebaseFirestore.getInstance().collection("Services")
                                .document(getSnapshots().getSnapshot(current).getId())
                                .set(service)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, context.getString(R.string.service_status_updated), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            });
        }
        holder.serviceRate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, final float rating, boolean fromUser) {
                service.setRate(rating);
                FirebaseFirestore.getInstance().collection("Services")
                        .document(getSnapshots().getSnapshot(current).getId())
                        .set(service)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(context, context.getString(R.string.rate_changed_to) + rating, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
//        holder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "Service No. " + (current + 1), Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @NonNull
    @Override
    public ServiceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_service_item, parent, false);
        context = parent.getContext();
        return new ServiceHolder(view);
    }

    class ServiceHolder extends RecyclerView.ViewHolder {
        private TextView serviceType;
        private TextView serviceDetails;
        private TextView serviceTime;
        private RatingBar serviceRate;
        private CheckBox serviceComplete;

        ServiceHolder(View view) {
            super(view);
            serviceType = view.findViewById(R.id.serviceType);
            serviceDetails = view.findViewById(R.id.serviceDetails);
            serviceTime = view.findViewById(R.id.serviceTime);
            serviceRate = view.findViewById(R.id.serviceRate);
            serviceComplete = view.findViewById(R.id.serviceComplete);
        }
    }
}
