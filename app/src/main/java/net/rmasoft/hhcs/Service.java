package net.rmasoft.hhcs;

public class Service {
    private String patientUID;
    private String doctorUID;
    private String time;
    private String location;
    private String reachDetails;
    private Float rate;
    private Boolean completed;
    private String mUID;

    public Service() {
    } // Needed for Firestore

    Service(String patientUID, String doctorUID, String location, String reachDetails, String time, Boolean completed) {
        this.patientUID = patientUID;
        this.doctorUID = doctorUID;
        this.location = location; // online | offline
        this.reachDetails = reachDetails; // Address | phoneNumber
        this.time = time;
        this.completed = completed;
    }

    public String getPatientUID() {

        return patientUID;
    }

    public void setPatientUID(String patientUID) {
        this.patientUID = patientUID;
    }

    public String getDoctorUID() {
        return doctorUID;
    }

    public void setDoctorUID(String doctorUID) {
        this.doctorUID = doctorUID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReachDetails() {
        return reachDetails;
    }

    public void setReachDetails(String reachDetails) {
        this.reachDetails = reachDetails;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getUID() {
        return mUID;
    }

    public void setUID(String mUID) {
        this.mUID = mUID;
    }
}
