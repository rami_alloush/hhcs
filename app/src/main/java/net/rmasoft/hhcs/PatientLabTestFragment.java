package net.rmasoft.hhcs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class PatientLabTestFragment extends Fragment {


    public PatientLabTestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_patient_lab_test, container, false);
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {

            ImageView labTestImageView = rootView.findViewById(R.id.labTestImagePatient);

            // Reference to an image file in Cloud Storage
            StorageReference storageReference = FirebaseStorage.getInstance().
                    getReference()
                    .child(currentUser.getUid() + ".jpg");

            // Download directly from StorageReference using Glide
            GlideApp.with(this)
                    .load(storageReference)
                    .into(labTestImageView);

            if (labTestImageView.getDrawable() == null) {
                TextView noImage = rootView.findViewById(R.id.noImage);
                noImage.setText(getString(R.string.no_labtests));
            }
        }

        return rootView;
    }

}
